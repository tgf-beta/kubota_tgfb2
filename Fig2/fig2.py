import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


def read_data(file: str, file_number: int) -> pd.DataFrame:
    """Read experiment csv dataset.

    Parameters
    ----------
    file : str
        filename
    file_number : int
        file number in order.

    Returns
    -------
    pd.DataFrame
        Dataframe column info:
        "volume": Metastatic tumor volume of A549 (10^5 um^3)
        "d0": Distance from A549 to alpha-SMA (10^2 um)
        "d1": Distance from A549 to VEGFR3 (10^2 um)
    """
    df = pd.read_csv(file)
    df.columns = ["file", "volume", "d0", "d1", "d2"]
    df["file"] = file_number
    return df


# Load data, use only D11 dataset.
files = [
    "190708_A549-mCh_Tb_D7_1D11iv_01x32_2_590_pCG_p50d2.csv",
    "190708_A549-mCh_Tb_D7_1D11iv_02x32_590_pCG_p50d2.csv",
    "190708_A549-mCh_Tb_D7_1D11iv_03x32_590_pCG_p50d2.csv",
    "190708_A549-mCh_Tb_D7_MsIgGiv_04x32_590_pCG_p50d2.csv",
    "190708_A549-mCh_Tb_D7_MsIgGiv_05x32_590_pCG_p50d2.csv",
    "190708_A549-mCh_Tb_D7_MsIgGiv_06x32_590_pCG_p50d2.csv",
]
df_list = [read_data(file, i + 1) for i, file in enumerate(files)]
df11 = pd.concat(df_list[0:3], axis=0, ignore_index=True)

# D1 X Volume
fig = sns.jointplot(
    "d1",
    "volume",
    data=df11,
    kind="kde",
    color="#E4007E",
    stat_func=None,
    size=10,
    ratio=6,
    space=0.1,
    xlim=(0, 500),
    ylim=(0, 100000),
)
plt.savefig("191210_A549-mCh_Tb_D7_1D11_jointplot01_demo.png")
plt.savefig("191210_A549-mCh_Tb_D7_1D11_jointplot01_demo.pdf")
plt.savefig("191210_A549-mCh_Tb_D7_1D11_jointplot01_demo.eps")

# D0 X Volume
fig = sns.jointplot(
    "d0",
    "volume",
    data=df11,
    kind="kde",
    color="#E4007E",
    stat_func=None,
    size=10,
    ratio=6,
    space=0.1,
    xlim=(0, 500),
    ylim=(0, 100000),
)
plt.savefig("191210_A549-mCh_Tb_D7_1D11_jointplot02_demo.png")
plt.savefig("191210_A549-mCh_Tb_D7_1D11_jointplot02_demo.pdf")
plt.savefig("191210_A549-mCh_Tb_D7_1D11_jointplot02_demo.eps")

# D0 X D1
sns.jointplot(
    "d1",
    "d0",
    data=df11,
    kind="kde",
    color="#E4007E",
    stat_func=None,
    size=10,
    ratio=6,
    space=0.1,
    xlim=(0, 500),
    ylim=(0, 100),
)
plt.savefig("191210_A549-mCh_Tb_D7_1D11_jointplot03_demo.png")
plt.savefig("191210_A549-mCh_Tb_D7_1D11_jointplot03_demo.pdf")
plt.savefig("191210_A549-mCh_Tb_D7_1D11_jointplot03_demo.eps")

# Making an ECDF X (d0 & d1) plot
df3 = df_list[2]

# Create a figure of size 8x6 inches, 80 dots per inch
fig = plt.figure(figsize=(7, 7), dpi=300)
ax = fig.add_subplot(111)

# ECDF on d0
d0_sorted = np.sort(df3["d0"])
ecdf_d0 = np.arange(1, len(d0_sorted) + 1) / len(d0_sorted)
ax.plot(d0_sorted, ecdf_d0, color="#21AB38", marker=".", linestyle="none", label="Distance from SMA")

# ECDF on d1
d1_sorted = np.sort(df3["d1"])
ecdf_d1 = np.arange(1, len(d1_sorted) + 1) / len(d1_sorted)
ax.plot(d1_sorted, ecdf_d1, color="#E4007E", marker=".", linestyle="none", label="Distance from VR3")

plt.margins(0.02)  # Keeps data off plot edges
plt.axis([0, 500, 0, 1])
plt.xlabel("Distance from A549 cells")
plt.ylabel("ECDF")
plt.savefig("191210_A549-mCh_Tb_D7_1D11_ECDF01_demo.pdf")
plt.savefig("191210_A549-mCh_Tb_D7_1D11_ECDF01_demo.eps")
plt.savefig("191210_A549-mCh_Tb_D7_1D11_ECDF01_demo.png")
